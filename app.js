const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;
const path = require("path");
const userRouter = require("./users/users.route");

// view engine setup
app.set("view engine", "ejs");

//app configuration
app.use(express.static(path.join(__dirname, "public")));
app.use(express.json());

// route
app.get("/", (req, res) => {
  res.sendFile("/index.html");
});
app.use("/users", userRouter);
app.listen(PORT, (err) => {
  if (err) console.log(err);
  console.log("Server listening on port " + PORT);
});
