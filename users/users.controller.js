const userModel = require("./users.model");

class UserController {
  // render register page
  renderRegister = (req, res) => {
    res.render("register");
  };

  //   render login page
  renderLogin = (req, res) => {
    res.render("login");
  };
  // get all users
  allUser = (req, res) => {
    const allUsers = userModel.getUsers();
    return res.json(allUsers);
  };

  // register new user
  registerUser = (req, res) => {
    let { username, email, password } = req.body;
    if (!username) {
      res.status(400);
      return res.json({ message: "Invalid username" });
    }
    if (!email) {
      res.status(400);
      return res.json({ message: "Invalid email" });
    }
    if (!password) {
      res.status(400);
      return res.json({ message: "Invalid password" });
    }
    if (password.length < 6) {
      res.status(400);
      return res.json({ message: "Password should be at least 6 characters" });
    }

    // find user already exist
    const existUser = userModel.isRegistered(username, email);
    if (existUser) {
      res.status(400);
      return res.json({ message: "Username or Email is already exists" });
    }
    res.status(200);
    userModel.recordUser(username, email, password);
    return res.json({ message: "Successfully adding new user" });
  };

  //   Login Authentication
  loginUser = (req, res) => {
    let { email, password } = req.body;
    const sortedUsers = userModel.isAuthenticated(email, password);
    if (sortedUsers) {
      res.status(200);
      return res.json(sortedUsers);
    } else {
      res.status(400);
      return res.json({ message: "Invalid credentials" });
    }
  };
}
module.exports = new UserController();
