const md5 = require("md5");
const users = [];

class UserModel {
  // get  user data
  getUsers = () => {
    return users;
  };

  //   check whether user already exist
  isRegistered = (username, email) => {
    const existUser = users.find((data) => {
      return data.username === username || data.email === email;
    });
    if (existUser) {
      return true;
    } else {
      return false;
    }
  };

  //   record new user
  recordUser = (username, email, password) => {
    users.push({
      id: users.length + 1,
      username: username,
      email: email,
      password: md5(password),
    });
  };

  // Login authentication
  isAuthenticated = (email, password) => {
    const sortedUsers = users.find((user) => {
      return user.email === email && user.password === md5(password);
    });
    return sortedUsers;
  };
}

module.exports = new UserModel();
