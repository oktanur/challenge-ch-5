const express = require("express");
const userRouter = express.Router();
const userController = require("./users.controller");
const path = require("path");

userRouter.get("/rock_paper_scs", (req, res) => {
  res.sendFile(path.join(__dirname, "../public/rock_paper_scs.html"));
});

userRouter.get("/", userController.allUser);
userRouter.get("/register", userController.renderRegister);
userRouter.get("/login", userController.renderLogin);
userRouter.post("/register", userController.registerUser);
userRouter.post("/login", userController.loginUser);

module.exports = userRouter;
